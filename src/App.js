import React, { useEffect, useState } from 'react';
import './App.css'
import Tmdb from './Tmdb.js';
import MovieRow from './components/movieRow';
import FeaturedMovie from './components/FeaturedMovie';
import Header from './components/Header';


export default () => {

  const [movieList, setMovieList] = useState([]);
  const [featuredData, setfeaturedData] = useState(null)
  const [blackHeader, setBlackHeader] = useState(false);


  useEffect(() => {
    const loadAll = async () => {
      // pegando a lista TOTAl
      let list = await Tmdb.getHomeList();
      setMovieList(list);

      // Pegando Featured
      let originals = list.filter(i => i.slug === 'originals');
      // Pegando aleatorio
      let randomChosen = Math.floor(Math.random() * (originals[0].items.results.length - 1));
      // Pegando especifico
      let chosen = originals[0].items.results[randomChosen];
      let chosenInfo = await Tmdb.getMovieInfo(chosen.id, 'tv');
      setfeaturedData(chosenInfo);

    }
    loadAll()
  }, []);
  useEffect(() => {
    const scrollListener = () => {
      if (window.scrollY > 10) {
        setBlackHeader(true);
      } else {
        setBlackHeader(false);
      }

    }

    window.addEventListener('scroll', scrollListener);
    return () => {
      window.removeEventListener('scroll', scrollListener);
    }

  }, []);
  return (
    <div className="page">

      <Header black={blackHeader} />

      {featuredData &&
        <FeaturedMovie item={featuredData} />
      }

      <section className="lists">
        {movieList.map((item, Key) => (

          <MovieRow Key={Key} title={item.title} items={item.items} />

        ))}

      </section>
      <footer>

        <div className="frase-autor">Feito com <span role="img" aria-label="coração"> <img className="ico-coracao" src="https://legado.justica.gov.br/imagens/coracao.png/@@images/image.png"/> </span> por Yury Tomaz</div>
        <a href="https://github.com/Yury-tomaz" className="img-git">
          <img src="https://cdn.icon-icons.com/icons2/1476/PNG/512/github_101792.png" />
          <span>Visite meu perfil no GitHub</span>
        </a>

        <div className="txt-tmdb">dados do site fornecido por <a href="https://www.themoviedb.org/"><img className="tmdb-img" src="https://www.themoviedb.org/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg" /></a></div>
      </footer>
    </div>
  )
}