import React from 'react';
import './FeaturedMovie.css';

export default ({ item }) => {
    console.log(item);

    let firstDate = new Date(item.first_air_date);
    let genres = [];
    for(let i in item.genres){
        genres.push( item.genres[i].name);
    }

    return (
        <section className="featured" style={{
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            backgroundImage: `url(https://image.tmdb.org/t/p/original${item.backdrop_path})`
        }} >
            <div className="factured--vertical" >
                <div className="feactured--horizontal">
                    <div className="feactured--name"> {item.original_name}</div>
                    <div className="feactured--info">
                        <div className="feactured--points">{item.vote_average} pontos</div>
                        <div className="feactured--year">{firstDate.getFullYear()}</div>
                        <div className="feactured--seasons">{item.number_of_seasons} temporada{item.number_of_seasons !== 1 ? 's' : ''}</div>
                        <div className="feactured--description">{item.overview}</div>
                        <div className="feactured--buttons">
                            <a href={`/watch/${item.id}`} className="featured--watchbutton">Assistir</a>
                            <a href={`/list/add/${item.id}`} className="featured--mylistbutton">+ minha lista</a>

                        </div>
                        <div className="feactured--genres"><strong>Gêneros:</strong>{genres.join(', ')}</div>
                    </div>
                    <div className="feactured--"></div>
                </div>
            </div>
        </section>
    );
}